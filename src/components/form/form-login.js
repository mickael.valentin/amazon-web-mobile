import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import { Link, withRouter, Redirect } from 'react-router-dom'

const axios = require('axios')

function FormLogin(props) {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const Login = () => {
    console.log(email)
    console.log(password)

    axios
      .post('https://easy-login-api.herokuapp.com/users/login', {
        username: email,
        password: password
      })
      .then(function(response) {
        const json = response.headers['x-access-token']
        const obj = JSON.stringify(json)
        console.log(obj)
        localStorage.setItem('x-access-token', obj)
        console.log(props)
        props.history.push('/items')
        props.setBoolean(true)
      })
      .catch(function(error) {
        console.log(error)
      })
  }

  return (
    <PrincipalWrapper>
      <SousTitre>Connexion. Déjà client(e) ?</SousTitre>
      <EmailInput
        placeholder='Email (tél. pour les comptes mobiles)'
        type='email'
        onChange={e => setEmail(e.target.value)}
      ></EmailInput>
      <PasswordInput
        placeholder='Mot de passe'
        type='password'
        onChange={e => setPassword(e.target.value)}
      ></PasswordInput>
      <ButtonConnexion onClick={() => Login()}>Continuer</ButtonConnexion>
      <TextWrapper>
        <Text>
          Lorsque vous vous identifiez, vous acceptez les
          <LinkTo href=''> Conditions générales de vente</LinkTo> d'Amazon.
          Veuillez consulter notre
          <LinkTo href=''>
            Notice Protection de vos Informations Personnelles
          </LinkTo>
          , notre <LinkTo href=''> Notice Cookies</LinkTo> et notre
          <LinkTo href=''>
            Notice Annonces publicitaires basées sur vos centres d'intérêt
          </LinkTo>
          .
        </Text>
        <LinkTo href=''>Avez-vous besoin d'aide ?</LinkTo>
      </TextWrapper>
    </PrincipalWrapper>
  )
}

const PrincipalWrapper = styled.div`
  flex-direction: column;
  display: flex;
  background-color: white;
  height: 420px;
  margin: 0px 15px 0px 15px;
  padding: 5px 15px 5px 15px;
`

const EmailInput = styled.input`
  margin-bottom: 10px;
  border-radius: 3px;
  padding: 10px;
  font-size: 15px;
`

const PasswordInput = styled.input`
  margin-bottom: 10px;
  border-radius: 3px;
  padding: 10px;
  font-size: 15px;
`

const ButtonConnexion = styled.button`
  background: #f0c14b;
  border-color: #a88734 #9c7e31 #846a29;
  border-radius: 3px;
  padding: 10px;
  font-size: 15px;
`

const SousTitre = styled.h3``

const TextWrapper = styled.div``

const Text = styled.p``

const LinkTo = styled.a`
  text-decoration: none;
  color: #0066c0;
`

export default FormLogin
