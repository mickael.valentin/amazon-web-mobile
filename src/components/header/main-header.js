import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import { Link, withRouter, Redirect } from 'react-router-dom'
import logo from '../../new-logo-amazon-blanc.png'
import logoPrime from '../../amazon-prime.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faTimes,
  faBars,
  faCartArrowDown,
  faSearch,
  faMapMarkerAlt
} from '@fortawesome/free-solid-svg-icons'

function MainHeader(props) {
  return (
    <Container>
      <PrincipalWrapper>
        <IconWrapper>
          <FontAwesomeIcon
            icon={faBars}
            size='2x'
            color='white'
          ></FontAwesomeIcon>
        </IconWrapper>
        <LogoWrapper>
          <Logo src={logo}></Logo>
        </LogoWrapper>
        <CartIconWrapper>
          <FontAwesomeIcon
            icon={faCartArrowDown}
            size='2x'
            color='white'
            onClick={e => {
              props.showModal()
            }}
          ></FontAwesomeIcon>
        </CartIconWrapper>
      </PrincipalWrapper>
      <SecondWrapper>
        <InputWrapper>
          <Input
            placeholder='Rechercher'
            value={props.searchTerm}
            onChange={props.handleChange}
          ></Input>
        </InputWrapper>
        <SearchWrapper>
          <FontAwesomeIcon
            icon={faSearch}
            size='2x'
            color='black'
          ></FontAwesomeIcon>
        </SearchWrapper>
      </SecondWrapper>
      <ThirdWrapper>
        <MapMarkerWrapper>
          <FontAwesomeIcon
            icon={faMapMarkerAlt}
            size='1x'
            color='white'
          ></FontAwesomeIcon>
        </MapMarkerWrapper>
        <TextWrapper>
          <Text>Sélectionnez votre adresse de livraison</Text>
        </TextWrapper>
      </ThirdWrapper>
      <FourthWrapper>
        <TextFourthWrapper>
          <TextFourth>{props.searchResults.length} résultats</TextFourth>
        </TextFourthWrapper>
        <LogoPrimeWrapper>
          <LogoPrime src={logoPrime}></LogoPrime>
        </LogoPrimeWrapper>
      </FourthWrapper>
    </Container>
  )
}

const Container = styled.div`
  background-color: #232f3e;
`

const PrincipalWrapper = styled.div`
  display: flex;
  flex-direction: row;
`

const FourthWrapper = styled.div`
  display: flex;
  flex-direction: row;
  background-color: #fff;
  border-bottom: 2px solid #ddd;
`

const LogoPrimeWrapper = styled.div``

const TextFourthWrapper = styled.div`
  border-right: 2px solid #ddd;
  width: 50%;
`

const LogoPrime = styled.img`
  padding-top: 4%;
  height: auto;
`

const TextFourth = styled.p`
  color: #555;
  text-align: center;
`

const SecondWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  margin-bottom: 12px;
`

const SearchWrapper = styled.div`
  background: #fcbb6a;
  border-radius: 3px;
  padding: 1%;
`

const MapMarkerWrapper = styled.div`
  margin: 12px;
`

const TextWrapper = styled.div``

const LogoWrapper = styled.div``

const ThirdWrapper = styled.div`
  display: flex;
  flex-direction: row;
  background-color: rgb(55, 71, 90);
`

const InputWrapper = styled.div`
  width: 86%;
`

const IconWrapper = styled.div`
  width: 80px;
  padding: 10px 10px 10px 10px;
`

const CartIconWrapper = styled.div`
  width: 80px;
  padding: 10px 10px 10px 10px;
`

const Logo = styled.img`
  max-width: 50%;
  height: auto;
  margin-top: 5%;
`

const Input = styled.input`
  height: 35px;
  width: 100%;
  margin-left: 12px;
  padding-left: 10px;
  border-radius: 3px;
  font-size: 15px;
`
const Text = styled.p`
  color: white;
  font-size: 13px;
`

export default MainHeader
