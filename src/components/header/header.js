import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import { Link, withRouter, Redirect } from 'react-router-dom'
import logo from '../../new-logo-amazon-blanc.png'

function Header() {
  return (
    <HeaderWrapper>
      <LogoWrapper>
        <Logo src={logo}></Logo>
      </LogoWrapper>
    </HeaderWrapper>
  )
}

const HeaderWrapper = styled.header`
  background-color: #232f3e;
`

const LogoWrapper = styled.div``

const Logo = styled.img`
  max-width: 40%;
  height: auto;
  margin: 3%;
`

export default Header
