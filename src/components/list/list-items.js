import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import ItemsJson from '../../items.json'
import logoPrime from '../../../src/amazon-prime-items.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faStar,
  faStarHalf,
  faStarHalfAlt
} from '@fortawesome/free-solid-svg-icons'

function ListItems(props) {
  const [cartItems, setCartItems] = React.useState([])

  const AddToCart = item => {
    const newElement = {
      id_article: item.id_article,
      title: item.title,
      price: item.price,
      nb_comments: item.nb_comments,
      image_url: item.image_url
    }
    setCartItems(oldArray => [...oldArray, newElement])
    console.log(cartItems)
  }

  // function searchFunction() {
  //   //console.log('test')
  //   const results = ItemsJson.filter(item =>
  //     item.title.toLowerCase().includes(searchTerm)
  //   )
  //   setSearchResults(results)
  // }

  return (
    <PrincipalWrapper>
      {props.searchResults.map(item => (
        <ItemWrapper key={item.id}>
          <LeftColumn>
            <ItemImageWrapper>
              <ItemImage src={item.image_url}></ItemImage>
            </ItemImageWrapper>
          </LeftColumn>
          <RightColumn>
            <ItemDetails>
              <TitleWrapper>
                <Title>{item.title}</Title>
              </TitleWrapper>
              <StarWrapper>
                <FontAwesomeIcon
                  icon={faStar}
                  size='1x'
                  color='#fcbb6a'
                ></FontAwesomeIcon>
                <FontAwesomeIcon
                  icon={faStar}
                  size='1x'
                  color='#fcbb6a'
                ></FontAwesomeIcon>
                <FontAwesomeIcon
                  icon={faStar}
                  size='1x'
                  color='#fcbb6a'
                ></FontAwesomeIcon>
                <FontAwesomeIcon
                  icon={faStarHalfAlt}
                  size='1x'
                  color='#fcbb6a'
                ></FontAwesomeIcon>
                {item.nb_comments}
              </StarWrapper>
              <PriceWrapper>
                <Price>{item.price}</Price>
              </PriceWrapper>
              <LogoPrimeWrapper>
                <LogoPrime src={logoPrime}></LogoPrime>
              </LogoPrimeWrapper>
              <TexteWrapper>
                <Texte>
                  Recevez-le <TexteSpan>samedi 21 mars</TexteSpan>. Livraison
                  GRATUITE par Amazon
                </Texte>
              </TexteWrapper>
              <ButtonWrapper>
                <Button onClick={() => AddToCart(item)}>
                  Ajouter au panier
                </Button>
              </ButtonWrapper>
            </ItemDetails>
          </RightColumn>
        </ItemWrapper>
      ))}
    </PrincipalWrapper>
  )
}

const PrincipalWrapper = styled.div``

const Button = styled.button`
  background: #f0c14b;
  border-color: #a88734 #9c7e31 #846a29;
  color: #111;
  padding: 5px;
  border-radius: 10px;
`

const ButtonWrapper = styled.div``

const ItemWrapper = styled.div`
  display: flex;
  padding-top: 5%;
  padding-bottom: 5%;
  border-bottom: 2px solid #ddd;
`

const ItemImageWrapper = styled.div`
  width: 100%;
  height: 150px;
`

const LeftColumn = styled.div`
  width: 40%;
`

const RightColumn = styled.div`
  display: flex;
  flex-direction: column;
  width: 50%;
`

const ItemImage = styled.img`
  max-width: 100%;
  height: auto;
`

const TitleWrapper = styled.div``

const Title = styled.h3``

const ItemDetails = styled.div`
  padding-left: 10%;
`

const Price = styled.h4``

const PriceWrapper = styled.div``

const StarWrapper = styled.div``

const TexteWrapper = styled.div``

const Texte = styled.p`
  color: #555;
  font-size: 14px;
`

const TexteSpan = styled.span`
  font-weight: bold;
`

const LogoPrimeWrapper = styled.div``

const LogoPrime = styled.img``

export default ListItems
