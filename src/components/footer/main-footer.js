import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import { Link, withRouter, Redirect } from 'react-router-dom'

function MainFooter(props) {
  return (
    <Container>
      <TextWrapper>
        <Text>HAUT DE LA PAGE</Text>
      </TextWrapper>
      <PrincipalWrapper>
        <LeftColumn>
          <List>
            <ElementList>
              <LinkTo href=''>Page d'accueil d'Amazon.fr</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Chez vous</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Vos commandes</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Votre Compte</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Vos listes</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Trouver une liste</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Trouver un cadeau</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>
                Articles que vous avez consultés récemment
              </LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Télécharger l'application d'Amazon</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Site Amazon pour ordinateur</LinkTo>
            </ElementList>
          </List>
        </LeftColumn>
        <RightColumn>
          <List>
            <ElementList>
              <OpenCart
                onClick={e => {
                  props.showModal()
                }}
              >
                Panier (<CartNumber>{props.cartItems.length}</CartNumber>)
              </OpenCart>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Service client</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Aide</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Vendre</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Devenez client Amazon business</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Vendez sur Amazon business</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Informations sur notre MarketpLace</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Gérer vos abonnements</LinkTo>
            </ElementList>
            <ElementList>
              <LinkTo href=''>Coordonnées 1-Click</LinkTo>
            </ElementList>
          </List>
        </RightColumn>
      </PrincipalWrapper>
    </Container>
  )
}

const Container = styled.div`
  background-color: #232f3e;
`
const PrincipalWrapper = styled.div`
  display: flex;
  flex-direction: row;
`

const LeftColumn = styled.div`
  display: flex;
  flex-direction: column;
  width: 50%;
`

const RightColumn = styled.div`
  display: flex;
  flex-direction: column;
  width: 50%;
`

const OpenCart = styled.span``

const CartNumber = styled.span`
  color: #e47911;
`

const TextWrapper = styled.div`
  padding: 10px;
  background: #37475a;
`

const Text = styled.p`
  text-align: center;
  font-size: 11px;
  color: #fff;
`

const List = styled.ul`
  list-style: none;
  color: #fff;
  font-size: 15px;
  padding-inline-start: 12px;
`

const ElementList = styled.li`
  margin-bottom: 10px;
`

const LinkTo = styled.a`
  text-decoration: none;
  color: white;
`

export default MainFooter
