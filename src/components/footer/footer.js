import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import { Link, withRouter, Redirect } from 'react-router-dom'

function Footer() {
  return (
    <FooterWrapper>
      <List>
        <ElementList>
          <LinkTo href=''>Conditions générales de vente</LinkTo>
        </ElementList>
        <ElementList>
          <LinkTo href=''>
            Conditions de participation au programme Marketplace
          </LinkTo>
        </ElementList>
        <ElementList>
          <LinkTo href=''>Vos informations personnelles</LinkTo>
        </ElementList>
        <ElementList>
          <LinkTo href=''>Cookies</LinkTo>
        </ElementList>
        <ElementList>
          <LinkTo href=''>Annonces basées sur vos centres d’intérêt</LinkTo>
        </ElementList>
        <ElementList>
          <Text>© 1996-2020 Amazon.com, Inc.</Text>
        </ElementList>
      </List>
    </FooterWrapper>
  )
}

const FooterWrapper = styled.footer`
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: #232f3e;
  padding-top: 10px;
  padding-bottom: 10px;
`

const List = styled.ul`
  list-style: none;
  text-align: center;
  color: #fff;
  padding-inline-start: 0px;
  font-size: 11px;
`

const ElementList = styled.li``

const LinkTo = styled.a`
  text-decoration: none;
  color: white;
`

const Text = styled.p``

export default Footer
