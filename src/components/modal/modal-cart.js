import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'

function ModalCart(props) {
  const DeleteItemCart = item => {
    const id = props.cartItems.indexOf(item.id_article)
    //const id = e.target.parentElement
    console.log(id)
    console.log(item.title)
    props.cartItems.splice(id, 1)
    props.setCartItems([...props.cartItems])
  }

  if (!props.modalBoolean) {
    return null
  }
  return (
    <Container>
      <ModalWrapper>
        <TitleWrapper>
          <Title>Mon Panier ({props.cartItems.length})</Title>
        </TitleWrapper>
        {props.cartItems.map(item => (
          <CartItemsListWrapper key={item.id}>
            <LeftColumn>
              <ItemImageWrapper>
                <ItemImage src={item.image_url}></ItemImage>
              </ItemImageWrapper>
            </LeftColumn>
            <RightColumn>
              <ItemDetails>
                <TitleItemsWrapper>
                  <TitleItems>{item.title}</TitleItems>
                </TitleItemsWrapper>
                <PriceWrapper>
                  <Price>{item.price}</Price>
                </PriceWrapper>
                <DeleteButtonWrapper>
                  <FontAwesomeIcon
                    icon={faTrashAlt}
                    size='1x'
                    color='black'
                    onClick={e => {
                      DeleteItemCart(item)
                    }}
                  ></FontAwesomeIcon>
                </DeleteButtonWrapper>
              </ItemDetails>
            </RightColumn>
          </CartItemsListWrapper>
        ))}
        <CloseButtonWrapper>
          <CloseButton
            onClick={e => {
              props.setBoolean(false)
            }}
          >
            Fermer
          </CloseButton>
        </CloseButtonWrapper>
      </ModalWrapper>
    </Container>
  )
}

const Container = styled.div`
  position: absolute;
  top: 20%;
  left: 5%;
  background-color: #fcbb6a;
  width: 90%;
`

const ModalWrapper = styled.div``

const TitleWrapper = styled.div``

const Title = styled.h4`
  text-align: center;
`

const CartItemsListWrapper = styled.div`
  display: flex;
  margin: 10%;
`

const CloseButtonWrapper = styled.div`
  text-align: center;
  margin-bottom: 8%;
`

const CloseButton = styled.button`
  background: #f0c14b;
  border-color: #a88734 #9c7e31 #846a29;
  padding: 5px;
  border-radius: 10px;
  font-size: 15px;
`

const ItemImageWrapper = styled.div``

const LeftColumn = styled.div`
  width: 30%;
`

const RightColumn = styled.div`
  display: flex;
  flex-direction: column;
  width: 70%;
`

const ItemImage = styled.img`
  max-width: 100%;
  height: auto;
`

const ItemDetails = styled.div`
  padding-left: 10%;
`

const Price = styled.h5``

const PriceWrapper = styled.div``

const TitleItemsWrapper = styled.div``

const TitleItems = styled.h5``

const DeleteButtonWrapper = styled.div``

export default ModalCart
