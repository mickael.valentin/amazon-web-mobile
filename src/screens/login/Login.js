import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import { Link, withRouter, Redirect } from 'react-router-dom'
import Header from '../../components/header/header'
import Footer from '../../components/footer/footer'
import FormLogin from '../../components/form/form-login'

function Login(props) {
  return (
    <LoginWrapper>
      <Header></Header>
      <TitreWrapper>
        <TitrePrincipal>Bienvenue</TitrePrincipal>
      </TitreWrapper>
      <FormLogin history={props.history}></FormLogin>
      <SpecificFooter>
        <Footer></Footer>
      </SpecificFooter>
    </LoginWrapper>
  )
}

const LoginWrapper = styled.div`
  height: 100vh;
  background-color: #f6f6f6;
`
const SpecificFooter = styled.div`
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
`

const TitreWrapper = styled.div`
  margin-left: 15px;
`

const TitrePrincipal = styled.h2``

export default withRouter(Login)
