import React, { useState, useEffect, Component } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from 'react-router-dom'
import Login from '../screens/login/Login'
import ItemsList from '../screens/items/Items'

export default function BasicExample() {
  const [boolean, setBoolean] = useState(false)

  const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      render={props =>
        localStorage.getItem('x-access-token') ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/', state: { from: props.location } }} />
        )
      }
    />
  )

  return (
    <Router>
      <div>
        <Switch>
          <Route setBoolean={setBoolean} exact path='/' component={Login} />
          <PrivateRoute exact path='/items/' component={ItemsList} />
          <Redirect to='/' />
        </Switch>
      </div>
    </Router>
  )
}
