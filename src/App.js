import React from 'react'
import logo from './logo.svg'
import './App.css'
import styled from 'styled-components'

import Router from './config/router'

function App() {
  return (
    <Body className='App'>
      <Router></Router>
    </Body>
  )
}

const Body = styled.body`
  height: 100vh;
`

export default App
